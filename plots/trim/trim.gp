reset
set terminal postscript eps enhanced monochrome font "Times,15"
set size 1,0.5
set output "trimgraph.eps"
set ylabel "Zeroed space [Gb]" offset 2,0
set xlabel "Allocated space on disk [Gb]" offset 0,0.5
set nokey

set xtics 0,10,64 nomirror
set xrange [0:60]
set mxtics 2

set ytics 0,5,30 nomirror
unset y2tics

set nox2tics
set noy2tics

set style rect fc lt -1 fs solid 0.15 noborder

set obj rect from 0,0 to 4,20
set obj rect from 16,0 to 20,20
set obj rect from 33,0 to 36,20
set obj rect from 49,0 to 53,20
set obj rect from 57,0 to 59,20

plot "logfiletrim" using 1:7 w p
!epstopdf trimgraph.eps && rm trimgraph.eps