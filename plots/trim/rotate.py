f=open("logfiletrim", "r")
d=f.read()
lines=d.splitlines()


for l in lines:
	s=l.split()
	
	#original data
	filled_gb = int(s[0])
	filled_percent = (int(s[0]) / 60.0 * 100 )

	#derived data
	zeroed_in_percent = float(s[1])
	zeroed_in_gb = (60.0 / 100 * float(s[1]))
	not_zeroed_in_percent = 100 -  zeroed_in_percent
	not_zeroed_in_gb = 60 - zeroed_in_gb
	trimmed_in_percent = filled_percent - not_zeroed_in_percent 
	trimmed_in_gb = filled_gb - not_zeroed_in_gb

	# s.append(zeroed_in_gb)
	# s.append(not_zeroed_in_percent)
	# s.append(not_zeroed_in_gb)
	# s.append(trimmed_in_percent)
	# s.append(trimmed_in_gb)

	#print s[0] + ' ' + s[1] + ' {0:.4g}'.format(s[2])
	print "%s %s %.4g %.4g %.4g %.4g %.4g" % (filled_gb,zeroed_in_percent,zeroed_in_gb,not_zeroed_in_percent, not_zeroed_in_gb,trimmed_in_percent,trimmed_in_gb)