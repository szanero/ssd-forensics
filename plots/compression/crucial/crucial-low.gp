reset
set terminal postscript eps enhanced monochrome font "Times,15"
set output "cruciallow.eps"

set nokey
set xlabel "Time [m:s]"
set ylabel "Throughput [MB/s]"
set xdata time
set grid
set timefmt "%s"
set yrange [0:]
set style line 2 lt 2 lw 5 lc rgb "blue"

set xtic 15 nomirror
set mxtic 5
set ytic nomirror

#set xrange [0:50]

INPUT_FILE = "low.final"

##dots + fit
plot INPUT_FILE using ($1*2+1):2:3 with yerrorbars, '' using ($1*2+1):2 with points pt 7, '' using ($1*2+1):2 smooth bezier

!epstopdf cruciallow.eps && rm cruciallow.eps