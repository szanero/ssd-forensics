reset
set terminal postscript eps enhanced monochrome font "Times,15"
set output "corsairhigh.eps"

set nokey
set xlabel "Time [m:s]"
set ylabel "Throughput [MB/s]"
set xdata time
set grid
set timefmt "%s"
set yrange [0:]
set style line 2 lt 2 lw 5 lc rgb "blue"

set xtic 15 nomirror
set mxtic 5
set ytic nomirror

INPUT_FILE = "high.final"

#set title "Write throughput, low entropy data"

##dots + fit
plot INPUT_FILE using ($1*2+1):2:3 with yerrorbars, '' using ($1*2+1):2 with points pt 7, '' using ($1*2+1):2 smooth bezier

!epstopdf corsairhigh.eps && rm corsairhigh.eps