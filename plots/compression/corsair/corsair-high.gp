reset
set terminal postscript eps enhanced monochrome font "Times,15"
set output "corsairhigh.eps"

set nokey
set xlabel "Time [m:s]"
set ylabel "Throughput [MB/s]"
set xdata time
set grid
set timefmt "%s"
set yrange [0:]
set style line 2 lt 2 lw 5 lc rgb "blue"

set xtic 30 nomirror
set mxtic 6
set ytic nomirror

INPUT_FILE_H = "high.final"

#set title "Write throughput, low entropy data"

##dots + fit
plot INPUT_FILE_H using ($1*5+2):2:3 with yerrorbars, '' using ($1*5+2):2 with points pt 7, '' using ($1*5+2):2 smooth bezier

!epstopdf corsairhigh.eps && rm corsairhigh.eps