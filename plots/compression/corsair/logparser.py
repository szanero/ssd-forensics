#pylint: disable-msg=C0103
#
import glob
from math import sqrt
from math import fabs


s = [] #sum
s2 = [] #sum sqared
N = 14


for files in glob.glob("*low.dat"): #change to low.dat to plot other data
    cont = 0
    with open( files, "r" ) as f:
        rows = f.read().splitlines()
    for pivot in xrange(2, len(rows), 5):
        chunk = rows[pivot - 2 : pivot + 3]
        
        interp_raw = 0
        for i in chunk:
            interp_raw += float(i.split()[1])
        
        interp_raw /= 5

        #num = float(row.split()[2]) #3rd column in the logfile
        try:
            s[cont] += ( interp_raw )
        except IndexError:
            s.append( interp_raw )
        try:
            s2[cont] += ( interp_raw * interp_raw )
        except IndexError:
            s2.append( interp_raw )    
        cont += 1

cont = 0


with open("low.final", "w+") as out:
    for el, el2 in zip (s, s2):
        var = (el2 - (el*el) / N) / N
        #var = (el2 / N) - ((el / N)*(el / N))

        # print mean and variance
        line_out = str(cont) + ' {0:.4g}'.format(el / N) + ' {0:.4g}'.format(sqrt(fabs(var)))
        out.write(line_out + "\n" )
        cont += 1
